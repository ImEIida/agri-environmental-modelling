*  Land degradation problem (Example of Uzbekistan)
*  Farmer can decide which crops to grow
*  Crop griwth (production function) depend on machinery used and soil nutrients
*  Certain crops result in soil nutrient loss (land degradation)
*  Dynamics in change in funds available for agricultural expenses and land degradation (nutrient status of soil)



option limrow = 0;
option limcol = 0;



*#######################################################################################
*                Defining sets (indices)

Set  t      Years
/
1*10        From 1 to 10 years
/
;



Set  a      Crop types
/
cotton      Cotton
wheat       Winter wheat
rice        Rice
corn        Maize
veget       Vegetables
/
;

Set   i     Resources (endowments) of farm
/
land        Farmland
tractor     Machinery
labor       Labor
nutr        Soil nutrients
pesticide   Pesticide used
/
;


set m       Crop management practice
/
m1*m3       Three crop growht management practice
/
;

Set   d     Discount rate
/
d1*d2       Two discount rates d1 and d2
/
;


*#######################################################################################
*                Defining data used in the model

Parameters
p_cost(a)        Crop costs (EUR per ha)
p_resoraval(i,t) Resources (inputs) available at farm
p_initfunds(t)   Amount of initial funds available at farm (EUR)
p_price(a)       Price of annual crops (EUR per ton)
p_soil(i,t)      Initial level of nutrients in the soil (per farm)
p_discountO      Value in discount rate not taken into account
p_discount(d)    Simulated discount rate (%)
;


*       Defining data for output prices of annual crops, USD per ton
*p_yield("cotton")  =  2.4  ;
*p_yield("wheat")   =  3.6  ;
*p_yield("rice")    =  4.5  ;
*p_yield("corn")    =  4.8  ;
*p_yield("veget")   =  7.5  ;

p_price("cotton")   =  36    ;
p_price("wheat")    =  227   ;
p_price("rice")     =  682   ;
p_price("corn")     =  227   ;
p_price("veget")    =  260   ;



*                Defining data for crop production costs, USD per ha
p_cost("cotton")    =  492   ;
p_cost("wheat")     =  492   ;
p_cost("rice")      =  1217  ;
p_cost("corn")      =  438   ;
p_cost("veget")     =  926   ;


*                Defining data for resources available at farm
p_resoraval("land",t)    = 80        ;
p_resoraval("tractor",t) = 8000      ;
p_resoraval("labor",t)   = 4000      ;


*                Amount of initial funds at farm, USD per farm
p_initfunds("1")         = 40000     ;


*                Initial levl of nutrients in the soil (per farm)
p_soil("nutr","1")       =  60       ;


*                Discount rates of farmer
p_discountO              =  0          ;
p_discount("d1")         =  0.02       ;
p_discount("d2")         =  0.10       ;




Table
p_cropinput(a,i)    Resources (inputs) required to produce crops
           land        labor
cotton      1           9.8
wheat       1           8.6
rice        1           10.3
corn        1           8.6
veget       1           8.9
;


Table
p_tranutcrop(a,m,i)    Management practice
                    tractor    nutr
cotton.m1            40        0.1
wheat.m1             45        0.1
rice.m1              40        0.1
corn.m1              50        0.1
veget.m1             35        0.1
cotton.m2            55        0.2
wheat.m2             60        0.2
rice.m2              57        0.3
corn.m2              65        0.2
veget.m2             50        0.3
cotton.m3            72        0.3
wheat.m3             80        0.3
rice.m3              75        0.5
corn.m3              82        0.3
veget.m3             65        0.5
;


Table
p_yield(a,m)    Crop yield depending on management practice (t per ha)
                 m1            m2        m3
cotton           1.8          2.4        4
wheat            2.2          3.5        5
rice             2.7          3.5        4
corn             2             4         4.8
veget            3             5         10
;


Table
p_yieldegrad(a,m)    Land degradation (soil productivity loss) depending on management practice
               m1             m2          m3
cotton         0.00          0.03         0.07
wheat          0.01          0.01         0.05
rice           0.05          0.4          0.15
corn           0             0.1          0.1
veget          0.02          0.06         0.15
;




*#########################################################################################
*                Defining model variables, equations

*                Defining variables of the model
Variables
v_object            Cbjective variable (in our case gross margins (USD))
v_pv(t)             Present value of farm profits (USD)
v_funds(t)          Funds available at farm for agricultural expenses (USD)
;


*                Defining positive variables of the model
Positive variable
v_area(a,m,t)       Crop area (crop area cannot be negative (hectare))
v_soildeg(i,t)      Nutrient in the soil after land degradation
;


*                Defining equations of the model
Equations
e_object            Objective function  equation (USD)
e_pv(t)             Present value of farm profits (USD)
e_resourc(i,t)      Resource constraint equation
e_funds(t)          Funds available at farm for agricultural expenses (USD)
e_expense1          Agricultural expenditure constraint in the initial year (USD)
e_expense(t)        Agricultural expenditure constraint over years (USD)
e_tractavail(t)     Tractor use depending on management practice of crops (litres)
e_soildegrad1       Nutrients remained in the soil after land degradation (nutrient depletion) from crop cultivation in the first year
e_soildegradT(t)    Nutrient remained in the soil after land degradation (nutrient depletion) from crop cultivation over years
e_yieldnutr1        Crop yield from soil nutrients in the first year
e_yieldnutrT(t)     Crop yield from soil nutrients over years
e_nutrbal(t)        Soil nutrients remained after degradation cannot be more than the nutrients that were available in the initial year
;




*#########################################################################################
*                Stating the model: farm model that maximizes total gross margins of farm

*                Objective function
e_object..
                   v_object
              =E=
                   sum(t, v_pv(t) )
;

*                Present value function
e_pv(t)..
                   v_pv(t)
              =E=
                   sum(a,
                   p_price(a) * sum(m, p_yield(a,m)  *  v_area(a,m,t)  )
                -  p_cost(a)  * sum(m, v_area(a,m,t) )
                )
                / (1+p_discountO)**ord(t)
;

*                Resource constraint equation
e_resourc(i,t)..
                   sum((a,m), p_cropinput(a,i) * v_area(a,m,t)  )
              =L=
                   p_resoraval(i,t)
;


e_tractavail(t)..
                   sum((a,m), p_tranutcrop(a,m,"tractor")  * v_area(a,m,t)  )
              =L=
                   p_resoraval("tractor",t)
;


*                        Soil nutrients used for crop production cannot be more than nutrients available in soil in the initial year
e_yieldnutr1..
                   sum((a,m), p_tranutcrop(a,m,"nutr")  * v_area(a,m,"1")  )
              =L=
                   p_soil("nutr","1")
;

*                        Soil nutrients used for crop production cannot be more than nutrients available in soil over years
e_yieldnutrT(t) $ (ord(t) gt 1)..
                   sum((a,m), p_tranutcrop(a,m,"nutr")  * v_area(a,m,t)  )
              =L=
                   v_soildeg("nutr",t)
;


*                        Nutrient in the soil after crop were cultivated (land degradation) in the first year
e_soildegrad1..
                              p_soil("nutr","1")
                -  sum((a,m), p_yieldegrad(a,m)  * v_area(a,m,"1")          )
              =E=
                   v_soildeg("nutr","1")
;

*                        Nutrient in the soil after crop were cultivated (land degradation) over years
e_soildegradT(t) $ (ord(t) gt 1)..
                               v_soildeg("nutr",t-1)
                -  sum((a,m),  p_yieldegrad(a,m)     * v_area(a,m,t)         )
              =E=
                   v_soildeg("nutr",t)
;



*                   Funds available at farm (cumulative over years)
e_funds(t)..
                   v_funds(t)
              =E=
                   v_pv(t)
                +  v_funds(t-1)
;

*                 Initial farm expenditure constraint
e_expense1..
                   p_initfunds("1")
              =G=
                   sum((a,m), p_cost(a) * v_area(a,m,"1")  )
;

*                 Farm expenditure constraint over years
e_expense(t) $ (ord(t) gt 1 )..
                   v_funds(t-1)
              =G=
                   sum((a,m),  p_cost(a) * v_area(a,m,t)   )
;





*         Stating the name of model and equations that needs to be modeled
Model LandDegradation
/
e_object
e_pv
e_resourc
e_funds
e_expense1
e_expense
e_tractavail
e_soildegrad1
e_soildegradT
e_yieldnutr1
e_yieldnutrT
/
;


*                Defining name of variables to export into excel
Parameters
s_pv(d,t)              Export to excel the present value (USD per farm)
s_area(a,m,d,t)        Export to excel the crop cultivation area (ha)
s_shadowres(i,d,t)     Export to excel shadow values of resources  (USD per 1 unit of resource)
s_soilnutrient(i,t,d)
;



*                Loop statement to       execute a group of statements for each element of a set
*       In that Loop the discount rate is revised in the model in accordance with the
*    d element of p_discount. In turn the variables would be saved in the d element of data.
LOOP(d,
p_discountO   =  p_discount(d)              ;


*                        Statement that solves the model (the model is mixed integer programming (MIP))
SOLVE LandDegradation  maximizing  v_object  using  NLP  ;


*                Present value
s_pv(d,t)             =  v_pv.L(t)            ;

*                Crop area
s_area(a,m,d,t)       =  v_area.L(a,m,t)      ;

*                Shadow price of resources
s_shadowres(i,d,t)   =   e_resourc.M(i,t)     ;

*     Closing Loop with ) and ;

s_soilnutrient(i,t,d) = v_soildeg.L(i,t)         ;

)
;


*                Exporting output to excel
* Command        Command     Name exported date     Name excel file      Name exported date
$libinclude      xldump       s_pv                  Results.xls           s_pv
$libinclude      xldump       s_area                Results.xls           s_area
$libinclude      xldump       s_shadowres           Results.xls           s_shadowres
$libinclude      xldump       s_soilnutrient        Results.xls           s_soilnutrient
